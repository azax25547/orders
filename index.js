const mongoose = require('mongoose');

const { app } = require('./app');

const PORT = process.env.PORT || 4002;

const start = async () => {
    try {
        await mongoose.connect(`mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_USERNAME_PASSWORD}@${process.env.MONGO_DB_URI}`, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })

        console.log('Connected to DB');
    } catch (err) {
        console.log(err);
    }

    app.listen(PORT, () => {
        console.log(`Listening on port ${PORT}`)
    })
}

start();
