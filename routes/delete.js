const express = require("express");
const { middlewares } = require('@azaxxc/common/src');

const router = express.Router();
const { Order } = require('../models/order');

router.delete('/api/orders', middlewares.authAPIs, async (req, res, next) => {

    const { userId } = req.query;
    try {
        const deletedOrders = await Order.deleteMany({
            userId
        })

        res.status(201).send(deletedOrders);
    } catch (err) {
        next(err);
    }

})

module.exports = {
    deleteOrderRouter: router
}