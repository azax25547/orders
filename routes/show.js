const express = require('express')
const { middlewares, errors } = require('@azaxxc/common/src');

const router = express.Router();
const { Order } = require('../models/order');

router.get('/api/orders/:id', middlewares.authAPIs, async (req, res, next) => {

    let order;
    const { userId } = req.body;
    try {
        order = await Order.findById(req.params.id).populate('recipe')
        if (!order)
            throw new errors.HTTPError("Recipe's not found");


        if (order.userId.toHexString() !== userId)
            throw new errors.HTTPError("Not Authorized", "", 403);


        res.send(order);


    } catch (err) {
        next(err)
    }

})


module.exports = { showOrderRouter: router }