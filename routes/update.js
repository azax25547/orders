const express = require('express')
const { middlewares, errors } = require('@azaxxc/common/src');

const router = express.Router();
const { Order } = require('../models/order');
const { HTTPError } = require('@azaxxc/common/src/errors/Http-error');

router.put('/api/orders/:id', middlewares.authAPIs, async (req, res, next) => {
    let { id } = req.params;
    let { ...details } = req.body;
    try {
        updatedOrder = await Order.findByIdAndUpdate(id, {
            ...details
        }, {
            useFindAndModify: false,
            new: true
        })

        // if (!updatedRecipe)
        //     throw new HTTPError("Unable to find Recipe on this ID");

        res.send(updatedOrder);
    } catch (err) {
        next(err)
    }
})


module.exports = { updateOrderRouter: router }