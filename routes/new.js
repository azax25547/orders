const router = require("express").Router();
const { middlewares } = require("@azaxxc/common/src");
const { HTTPError } = require("@azaxxc/common/src/errors/Http-error");
const { Order } = require("../models/order");
const { Recipe } = require("../models/recipe");

router.post('/api/orders',
    middlewares.authAPIs,
    async (req, res, next) => {
        const { userId, recipeId } = req.body;
        let newOrder;
        try {
            let recipe = await Recipe.findById(recipeId);
            if (!recipe)
                throw new HTTPError("Ticket Not found!");

            newOrder = await Order.create({
                userId,
                recipe,
            })

            await newOrder.save();

            res.send(newOrder);
        } catch (err) {
            next(err)
        }

    })

module.exports = { createOrderRouter: router };