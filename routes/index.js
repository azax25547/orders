const express = require('express')
const { middlewares } = require('@azaxxc/common/src');
const { Order } = require('../models/order');
const { HTTPError } = require('@azaxxc/common/src/errors/Http-error');

const router = express.Router();
// middlewares.authAPIs, 
router.get('/api/orders', middlewares.authAPIs, async (req, res, next) => {
    const { userId } = req.query;
    try {
        const orders = await Order.find({ userId }).populate('recipe')
        if (!orders)
            throw new HTTPError("Unable to fetch data from DB")

        res.send(orders);
    } catch (err) {
        console.log(err)
        next(err)
    }

})


module.exports = { indexOrderRoute: router }