const request = require("supertest");
const { app } = require("../../app");
const { Recipe } = require("../../models/recipe");
var objectid = require('objectid');

describe('Fetching Order', () => {
    const ourUser = objectid();
    it('fetches the order', async () => {
        const recipe = await Recipe.create({
            price: 100,
            quantity: 2,
            recipe: 'Biriyani'
        })

        await recipe.save();

        const user = global.signin();

        const { body: order } = await request(app)
            .post('/api/orders')
            .set('x-access-token', user['x-access-token'])
            .send({
                userId: ourUser,
                status: "ordered",
                recipeId: recipe.id
            })
            .expect(200)

        console.log(order);

        const { body: fetchedOrder } = await request(app)
            .get(`/api/orders/${order.id}`)
            .set('x-access-token', user['x-access-token'])
            .send({
                userId: ourUser
            })
            .expect(200)
        console.log(fetchedOrder);

        expect(fetchedOrder.id).toEqual(order.id);
    })

    it("returns error if one user tries to fetch other user's order", async () => {
        const recipe = await Recipe.create({
            price: 100,
            quantity: 2,
            recipe: 'Biriyani'
        })

        await recipe.save();

        const user = global.signin();

        const { body: order } = await request(app)
            .post('/api/orders')
            .set('x-access-token', user['x-access-token'])
            .send({
                userId: ourUser,
                status: "ordered",
                recipeId: recipe.id
            })
            .expect(200)

        await request(app)
            .get(`/api/orders/${order.id}`)
            .set('x-access-token', global.signin()['x-access-token'])
            .send({
                userId: objectid()
            })
            .expect(403)

    })
})

