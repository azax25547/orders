const request = require("supertest");
const { app } = require("../../app");
const { Recipe } = require("../../models/recipe");
var objectid = require('objectid');

describe("Updating Order", () => {

    const ourUser = objectid();


    it("updates the order", async () => {
        const recipe = await Recipe.create({
            price: 120,
            quantity: 2,
            recipe: "Dosa"
        });

        await recipe.save();

        const user = global.signin();

        const { body: order } = await request(app)
            .post('/api/orders')
            .set('x-access-token', user['x-access-token'])
            .send({
                userId: ourUser,
                status: "ordered",
                recipeId: recipe.id
            })
            .expect(200)

        const { body: updatedOrder } = await request(app)
            .put(`/api/orders/${order.id}`)
            .set('x-access-token', user['x-access-token'])
            .send({
                status: "Accepted"
            })


        expect(updatedOrder.status).toEqual("Accepted");
    })

})