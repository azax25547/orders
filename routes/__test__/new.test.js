const request = require("supertest");
const { app } = require("../../app");
const { Recipe } = require("../../models/recipe");
var objectid = require('objectid');

const user = global.signin();

it("returns an error if the recipe does not exist", async () => {
    const recipeId = objectid();
    await request(app)
        .post('/api/orders')
        .set('x-access-token', user['x-access-token'])
        .send({
            userId: objectid(),
            recipeId: recipeId.toHexString()
        })
        .expect(404)
})

it("creates an order", async () => {
    const recipe = await Recipe.create({
        price: 100,
        quantity: 2,
        recipe: 'Biriyani'
    })

    await recipe.save();

    await request(app)
        .post('/api/orders')
        .set('x-access-token', user['x-access-token'])
        .send({
            userId: objectid(),
            status: "ordered",
            recipeId: recipe.id
        })
        .expect(200)

})
