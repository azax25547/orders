const request = require("supertest");
const { app } = require("../../app");
const { Recipe } = require("../../models/recipe");

var objectid = require('objectid');

const buildRecipe = async () => {
    const recipe = await Recipe.create({
        price: 100,
        quantity: 2,
        recipe: 'Biriyani'
    })

    await recipe.save();

    return recipe;

}

it('fetches orders for an particular user', async () => {
    // create three recipes
    const recipeOne = await buildRecipe();
    const recipeTwo = await buildRecipe();
    const recipeThree = await buildRecipe();

    const userOne = global.signin();
    const userTwo = global.signin();
    const userTwoObject = objectid()
    // console.log(userOne['x-access-token']);

    await request(app)
        .post('/api/orders')
        .set('x-access-token', userOne['x-access-token'])
        .send({
            userId: objectid(),
            status: "ordered",
            recipeId: recipeOne.id
        })
        .expect(200)
    const { body: orderOne } = await request(app)
        .post('/api/orders')
        .set('x-access-token', userTwo['x-access-token'])
        .send({
            userId: userTwoObject,
            status: "ordered",
            recipeId: recipeTwo.id
        })
        .expect(200)

    const { body: orderTwo } = await request(app)
        .post('/api/orders')
        .set('x-access-token', userTwo['x-access-token'])
        .send({
            userId: userTwoObject,
            status: "ordered",
            recipeId: recipeThree.id
        })
        .expect(200)


    const response = await request(app)
        .get('/api/orders')
        .query({
            userId: userTwoObject.toHexString()
        })
        .set('x-access-token', userTwo['x-access-token'])
        .expect(200)


    expect(response.body[0].userId).toBe(orderOne.userId)
    expect(response.body[1].userId).toBe(orderTwo.userId)

})