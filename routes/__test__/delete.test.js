const request = require("supertest");
const { app } = require("../../app");
const { Recipe } = require("../../models/recipe");
var objectid = require('objectid');
const user = global.signin();

async function getOrders(userId) {

    return await request(app)
        .get('/api/orders')
        .query({
            userId
        })
        .set('x-access-token', user['x-access-token'])
        .expect(200)
}

describe("deletes order", () => {
    const ourUser = objectid();
    it("should delete all orders", async () => {

        const recipe = await Recipe.create({
            price: 120,
            quantity: 2,
            recipe: "Dosa"
        });

        const anotherRecipe = await Recipe.create({
            price: 100,
            quantity: 1,
            recipe: "Biriyani"
        })

        await recipe.save();
        await anotherRecipe.save();


        const { body: orderOne } = await request(app)
            .post('/api/orders')
            .set('x-access-token', user['x-access-token'])
            .send({
                userId: ourUser,
                recipeId: recipe.id
            })
            .expect(200)

        const { body: orderTwo } = await request(app)
            .post('/api/orders')
            .set('x-access-token', user['x-access-token'])
            .send({
                userId: ourUser.toHexString(),
                recipeId: recipe.id
            })
            .expect(200)

        let orders;


        const { body: orderDeleted } = await request(app)
            .delete('/api/orders')
            .set('x-access-token', user['x-access-token'])
            .query({
                userId: ourUser.toHexString()
            })

        orders = await getOrders(ourUser.toHexString())

        expect(orders.body).toEqual([]);
        expect(orderDeleted.deletedCount).toEqual(2);

    })
})