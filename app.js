const express = require('express');
const { json } = require('body-parser');
const session = require("express-session");
const cors = require('cors');
const { middlewares } = require('@azaxxc/common/src');
require("dotenv").config();

const { createOrderRouter } = require('./routes/new')
const { showOrderRouter } = require('./routes/show')
const { indexOrderRoute } = require('./routes/index')
const { updateOrderRouter } = require('./routes/update')
const { deleteOrderRouter } = require('./routes/delete')

const app = express();
app.set('trust proxy', true);
app.use(json())

app.use(cors(
    {
        origin: "http://localhost:3000", // allow to server to accept request from different origin
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
        credentials: true, // allow session cookie from browser to pass through
    }
))

app.use(session({
    cookie: { maxAge: 60000000 },
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false
}))


app.use(createOrderRouter);
app.use(indexOrderRoute);
app.use(showOrderRouter);
app.use(updateOrderRouter);
app.use(deleteOrderRouter);

app.use(middlewares.errorhandler);

module.exports = { app };