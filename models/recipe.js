const mongoose = require('mongoose');

const RecipeSchema = new mongoose.Schema({
    recipe: {
        type: String
    },
    price: Number,
    quantity: Number,
}, {
    toJSON: {
        transform(doc, ret) {
            ret.id = ret._id;
            delete ret._id;
        }
    }
})


const Recipe = new mongoose.model("Recipe",RecipeSchema);

module.exports = {
    Recipe
}
